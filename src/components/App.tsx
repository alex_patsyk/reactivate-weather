import React, { Component } from 'react';
import { colors } from '../constants'

interface State {
    latitude: null,
    longitude: null,
    dataWeather: {
      weather: Array<{
        id: number,
        main: string,
        description: string,
        icon: string,
      }>,
      main: {
        temp: number,
        feels_like: number,
        temp_min: number,
        temp_max: number,
        pressure: number,
        humidity: number,
      },
      timezone: number,
      id: number,
      name: string,
      cod: number,
    },
    textSearch: string,
}

export default class App extends Component {

  state: State = {
    latitude: null,
    longitude: null,
    dataWeather: null,
    textSearch: null,
  }

  componentDidMount() {
    this.position()    
  }

  find = ():void => {
    const { textSearch } = this.state    
    fetch(`${process.env.REACT_APP_WEATHER_API_URL}data/2.5/weather?q=${textSearch}&units=metric&appid=${process.env.REACT_APP_WEATHER_KEY}`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText)
        return res.json()
      })
      .then(data => {
        this.setState({ dataWeather: data })        
      })
      .catch(error => {
        window.alert(`${error} please try later`)        
      });
  }

  change = (e:any):void => {
    e.preventDefault()
    this.setState({textSearch: e.target.value})
  }

  position = (): void => {
    navigator.geolocation.getCurrentPosition(      
      position => {
        fetch(`${process.env.REACT_APP_WEATHER_API_URL}data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=metric&appid=${process.env.REACT_APP_WEATHER_KEY}`)        
        .then(res => {
          if (!res.ok) throw Error(res.statusText)
          return res.json()
        })
        .then(data => {
          this.setState({ dataWeather: data })          
        })
        .catch(error => {
          window.alert(`${error} please try later`)          
        });
      },
      err => console.log(err),
      { enableHighAccuracy: true }
    )
  }

  render() {    
    const { dataWeather } = this.state    
    if( dataWeather === null)  { 
      return false
    }

    const { temp } = this.state.dataWeather.main
    let requiredColor: any
    const receivedTemp = Math.round(temp)
    if(receivedTemp < -10) {
      requiredColor = colors.filter(item => item.id === -10)
    } else if (receivedTemp > 30) {
      requiredColor = colors.filter(item => item.id === 30)
    } else {
      requiredColor = colors.filter(item => item.id === receivedTemp)
    }
    
    return (
      <div className="app" style={{backgroundColor: `${requiredColor[0].color}`}}> 
        { dataWeather && [
          <div className="search" key="search">
            <input type="text" className="search__input" onChange={this.change} />
            <button className="search__button" onClick={this.find}>Search</button>
          </div>,
          <div className="container" key="container">    
            {<p>{dataWeather.name}</p>}
            <img src={`http://openweathermap.org/img/wn/${dataWeather.weather[0].icon}@2x.png`} alt="weather icon" />
            <p>{temp}&#176;</p>
          </div>
        ]}
      </div>
    )
  }
}
